using MediatR;

namespace CQRS_WithMediatr.Infrastructure.CQRS.Commands
{
    public interface ICommand : IRequest
    {

    }
}